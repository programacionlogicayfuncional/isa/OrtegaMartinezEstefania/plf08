(ns plf08.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plf08.core :refer :all]
            [plf08.cuadradopolibio :as cpoli]
            [plf08.transposicionsimplenuevo :as tsimple]))

;test de cuadrado de polibio

(deftest cifrar-test
  (testing "Cifrado de 1 sola letra"
    (is (= "♞♞" (cpoli/cifrar "í")))
    (is (= "♚♕" (cpoli/cifrar "+")))
    (is (= "♗♜" (cpoli/cifrar "5")))
    (is (= "♔♜" (cpoli/cifrar "Q")))
    (is (= "♝♗" (cpoli/cifrar "ü")))
    (is (= "♚♛" (cpoli/cifrar "&"))))
  (testing "Cifrado de palabras"
    (is (= "♜♔♞♕♞♚♜♜" (cpoli/cifrar "hola")))
    (is (= "♕♜♜♜♞♘♞♘♛♞" (cpoli/cifrar "Fanny")))
    (is (= "♗♖♔♔♔♚♕♖" (cpoli/cifrar "AZUL")))
    (is (= "♗♖♞♖♜♜♝♝♞♜♞♚♞♚♞♕" (cpoli/cifrar "Amarillo")))
    (is (= "♞♖♞♕♝♝♜♜♜♚♞♕" (cpoli/cifrar "morado")))
    (is (= "♛♚♗♚♗♚♗♝" (cpoli/cifrar "1997"))))
  (testing "Cifrado de cadenas"
    (is (= "♕♘♜♖ ♜♕♝♖♝♛♝♚♜♜ ♞♚♜♜ ♞♚♞♚♝♖♝♕♞♜♜♜" (cpoli/cifrar "Me gusta la lluvia")))
    (is (= "♕♘♞♜ ♜♛♞♕♞♚♞♕♝♝ ♜♗♜♜♝♕♞♕♝♝♞♜♝♚♞♕ ♜♖♝♛ ♜♖♞♚ ♜♜♞♖♜♜♝♝♞♜♞♚♞♚♞♕" (cpoli/cifrar "Mi color favorito es el amarillo")))
    (is (= "♗♘♞♚♜♜♞♘♜♛♜♜♞♘♞♜♜♖♝♕♜♖♝♛ ♛♞ ♞♚♞♕♝♛ ♗♝ ♜♖♞♘♜♜♞♘♞♕♝♛" (cpoli/cifrar "Blancanieves y los 7 enanos")))))

(deftest descifrar-test
  (testing "Descifrado de 1 sola letra"
    (is (= "í" (cpoli/descifrar "♞♞")))
    (is (= "+" (cpoli/descifrar "♚♕")))
    (is (= "5" (cpoli/descifrar "♗♜")))
    (is (= "Q" (cpoli/descifrar "♔♜")))
    (is (= "ü" (cpoli/descifrar "♝♗")))
    (is (= "&" (cpoli/descifrar "♚♛"))))
  (testing "Cifrado de palabras"
    (is (= "hola" (cpoli/descifrar "♜♔♞♕♞♚♜♜")))
    (is (= "Fanny" (cpoli/descifrar "♕♜♜♜♞♘♞♘♛♞")))
    (is (= "AZUL" (cpoli/descifrar "♗♖♔♔♔♚♕♖")))
    (is (= "Amarillo" (cpoli/descifrar "♗♖♞♖♜♜♝♝♞♜♞♚♞♚♞♕")))
    (is (= "morado" (cpoli/descifrar "♞♖♞♕♝♝♜♜♜♚♞♕")))
    (is (= "1997" (cpoli/descifrar "♛♚♗♚♗♚♗♝"))))
  (testing "Cifrado de cadenas"
    (is (= "Me gusta la lluvia" (cpoli/descifrar "♕♘♜♖ ♜♕♝♖♝♛♝♚♜♜ ♞♚♜♜ ♞♚♞♚♝♖♝♕♞♜♜♜")))
    (is (= "Mi color favorito es el amarillo" (cpoli/descifrar "♕♘♞♜ ♜♛♞♕♞♚♞♕♝♝ ♜♗♜♜♝♕♞♕♝♝♞♜♝♚♞♕ ♜♖♝♛ ♜♖♞♚ ♜♜♞♖♜♜♝♝♞♜♞♚♞♚♞♕")))
    (is (= "Blancanieves y los 7 enanos" (cpoli/descifrar "♗♘♞♚♜♜♞♘♜♛♜♜♞♘♞♜♜♖♝♕♜♖♝♛ ♛♞ ♞♚♞♕♝♛ ♗♝ ♜♖♞♘♜♜♞♘♞♕♝♛")))))

;test de transposición columnar simple

(deftest cifrado-tcs-test
  (testing "Cifrar con la palabra clave"
    (is (= "h ts lkeeby   sui " (tsimple/cifrado-tcs "camerinos" "the sky is blue")))
    (is (= "hs  u t ysl ekibe " (tsimple/cifrado-tcs "cat" "the sky is blue")))
    (is (= "te h gear nesdek nyo  c ie sa  n b  li us " (tsimple/cifrado-tcs "abcdefghijklmn" "the sky is blue and ocean is green")))
    (is (= "a1s2d5a n7 6f d s f   s d f w e 1 2 3 1 2   3 1 s3d6j82 3 " (tsimple/cifrado-tcs "abcd123fghijklmnopqrstuvxyz,/" "asdasdjn fdsf sdfwe12312 3123125 36876"))))
  (testing "Cifrar con palabras clave de números"
    (is (= "fog  Ma rr os,soiee oo    nel r ne ltls cr  aiveoo" (tsimple/cifrado-tcs "1928374650" "Mi color favorito no es el negro, es el rosa")))
    (is (= "Mcofotneenr   sioraroosleoeera l vi    g,slo " (tsimple/cifrado-tcs "123" "Mi color favorito no es el negro, es el rosa")))
    (is (= "Eous jlzaso ealar i  s  csyon  e rosl l sa" (tsimple/cifrado-tcs "0987654" "El cielo es azul y las rosas son rojas")))
    (is (= "Ela ooa  uaa  e yrsj is   o lozlsnscelssr " (tsimple/cifrado-tcs "384965" "El cielo es azul y las rosas son rojas"))))
  (testing "Cifrar con palabras clave de simbolos"
    (is (= "Eec l ef h rdomííyuo hh dao a c " (tsimple/cifrado-tcs "!#$%&/()" "El día de hoy hace mucho frío")))
    (is (= "dcf  a  aho í h dyc lhmoE  í ou eer " (tsimple/cifrado-tcs ")(/&%$#!?" "El día de hoy hace mucho frío"))))
  (testing "Cifrar La misma palabra clave con mayúsculas y minúsculas"
    (is (= "i igiMel abem  ns sa eeulmL a o ytrrseb " (tsimple/cifrado-tcs "CAMIONES" "Mi nombre es Leslie y me gusta bailar")))
    (is (= "i igiMel abem  ns sa eeulmL a o ytrrseb " (tsimple/cifrado-tcs "camiones" "Mi nombre es Leslie y me gusta bailar")))
    (is (= "i igiMel abem  ns sa eeulmL a o ytrrseb " (tsimple/cifrado-tcs "cAmIoNeS" "Mi nombre es Leslie y me gusta bailar")))))

(deftest descifrado-tcs-test
  (testing "decodificar con la palabra clave"
    (is (= "the sky is blue   " (tsimple/descifrado-tcs "camerinos" "h ts lkeeby   sui ")))
    (is (= "the sky is blue   " (tsimple/descifrado-tcs "cat" "hs  u t ysl ekibe ")))
    (is (= "the sky is blue and ocean is green        " (tsimple/descifrado-tcs "abcdefghijklmn" "te h gear nesdek nyo  c ie sa  n b  li us ")))
    (is (= "asdasdjn fdsf sdfwe12312 3123125 36876       " (tsimple/descifrado-tcs "transmile" "ds21  w18 jd 3 nf36 ds2  af32 sd136s 15 afe27")))
    (is (= "asdasdjn fdsf sdfwe12312 3123125 36876                    " (tsimple/descifrado-tcs "abcd123fghijklmnopqrstuvxyz,/" "a1s2d5a n7 6f d s f   s d f w e 1 2 3 1 2   3 1 s3d6j82 3 "))))
  (testing "Cifrar con palabras clave de números"
    (is (= "Mi color favorito no es el negro, es el rosa      " (tsimple/descifrado-tcs "1928374650" "fog  Ma rr os,soiee oo    nel r ne ltls cr  aiveoo")))
    (is (= "Mi color favorito no es el negro, es el rosa " (tsimple/descifrado-tcs "123" "Mcofotneenr   sioraroosleoeera l vi    g,slo ")))
    (is (= "El cielo es azul y las rosas son rojas    " (tsimple/descifrado-tcs "0987654" "Eous jlzaso ealar i  s  csyon  e rosl l sa")))
    (is (= "El cielo es azul y las rosas son rojas    " (tsimple/descifrado-tcs "384965" "Ela ooa  uaa  e yrsj is   o lozlsnscelssr "))))
  (testing "Cifrar con palabras clave de simbolos"
    (is (= "El día de hoy hace mucho frío   " (tsimple/descifrado-tcs "!#$%&/()" "Eec l ef h rdomííyuo hh dao a c ")))
    (is (= "El día de hoy hace mucho frío       " (tsimple/descifrado-tcs ")(/&%$#!?" "dcf  a  aho í h dyc lhmoE  í ou eer "))))
  (testing "Cifrar La misma palabra clave con mayúsculas y minúsculas"
    (is (= "Mi nombre es Leslie y me gusta bailar   " (tsimple/descifrado-tcs "CAMIONES" "i igiMel abem  ns sa eeulmL a o ytrrseb ")))
    (is (= "Mi nombre es Leslie y me gusta bailar   " (tsimple/descifrado-tcs "camiones" "i igiMel abem  ns sa eeulmL a o ytrrseb ")))
    (is (= "Mi nombre es Leslie y me gusta bailar   " (tsimple/descifrado-tcs "cAmIoNeS" "i igiMel abem  ns sa eeulmL a o ytrrseb ")))))