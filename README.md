# plf08

FIXME: description

## Descripción del proyecto.

Esta aplicación realiza el cifrado de un texto plano por medio de codificación por cuadrado de polibio y transposición columnar simple.

En cuadrado de polibio se toma un archivo de texto plano y se codifica con piezas de ajedrez, al decodificar el texto ya codificado cambia las piezas de ajedrez por las letras, símbolos y números del texto original.

Con transposición columnar simple se recibe una palabra clave de longitud mayor o igual a 8, en caso de ser menor la codificación no se realiza, el texto a codificar y/o decodificar también es un archivo de texto plano.

## Forma de ejecutar.

Desde la terminal de windows o visual estudio se escribe lein run "metodo" "operacion" "archivoa" "archivob" "palabra"

## Opciones para su ejecución.

metodo corresponde al método de cifrado que quieres realizar, si es cuadrado de polibio o transposición columnar simple

operacion es cifrado o descifrado

archivoa es el archivo de texto plano que se lee

archivob es el archivo .txt en donde se va a guardar el texto cifrado o descifrado

palabra es la palabra clave, esta solo se utiliza para el cifrado y descifrado por transposición columnar simple

## Ejemplos de uso o ejecución.

CIFRADO POR MEDIO DE CUADRADO DE POLIBIO
lein run "cuadrado de polibio" "cifrado" "C:/Users/Fanny/plf08/resources/polibio/kafka_cifrado.txt" "C:/Users/Fanny/plf08/resources/polibio/kafkaoriginal.txt" "camion"

DESCIFRADO POR MEDIO DE CUADRADO DE POLIBIO
lein run "cuadrado de polibio" "descifrado" "C:/Users/Fanny/plf08/resources/polibio/kafka_descifrado.txt" "C:/Users/Fanny/plf08/resources/polibio/kafka_cifrado.txt" "hola"

CIFRADO POR MEDIO DE TRANSPOSICION SIMPLE
lein run "transposicion simple" "cifrado" "C:/Users/Fanny/plf08/resources/transposicion/kafka_cifrado.txt" "C:/Users/Fanny/plf08/resources/transposicion/kafkaoriginal.txt" "camiones"      

DESCIFRADO POR MEDIO DE TRANSPOSICION SIMPLE
lein run "transposicion simple" "descifrado" "C:/Users/Fanny/plf08/resources/transposicion/kafka_descifrado.txt" "C:/Users/Fanny/plf08/resources/transposicion/kafka_cifrado.txt" "camiones"

## Errores, limitantes, situaciones no consideradas o particularidades de tu solución.

En el método cuadrado de polibio hay un error de dos signos, ¿¡ ya que están juntos con el que le corresponde en el cuadrado, 
por lo que al momento de descifrar regresa los símbolos !? aunque debería de regresar ¡¿ dado que en el mapa las llaves se repiten. También, al momento de descifrar todos los espacios se convierten a espacios doble, ya que para decodificar se deben de tomar de 2 en 2 figuras de ajedrez y al hacer esto con un espacio todos los elementos se recorren provocando una pérdida de datos considerable, una vez hecha la descifración los espacios doble se regresan a un solo espacio para que el texto sea igual al original.

En el de transformacion columnar al momento de repetir una letra en la palabra clave no las toma en cuenta por lo que esa implementacion a posicionesletras falto, y como se respetan los caracteres que no estan dentro del alfabeto se completan con espacios para que los vectores dentro de los mapas sean del mismo tamaño y pueda dar solucion al cifrado y decifrado.

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar plf08-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
