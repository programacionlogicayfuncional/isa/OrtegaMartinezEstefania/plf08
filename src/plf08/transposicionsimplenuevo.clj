(ns plf08.transposicionsimplenuevo
  (:require [clojure.string :as str]))

(defn clave
  []
  (hash-map
   \a	0
   \á	1
   \b	2
   \c	3
   \d	4
   \e	5
   \é	6
   \f	7
   \g	8
   \h	9
   \i	10
   \í	11
   \j	12
   \k	13
   \l	14
   \m	15
   \n	16
   \ñ	17
   \o	18
   \ó	19
   \p	20
   \q	21
   \r	22
   \s	23
   \t	24
   \u	25
   \ú	26
   \ü	27
   \v	28
   \w	29
   \x	30
   \y	31
   \z	32
   \0	33
   \1	34
   \2	35
   \3	36
   \4	37
   \!	38
   \"	39
   \#	40
   \$	41
   \%	42
   \&	43
   \'	44
   \(	45
   \)	46
   \*	47
   \+	48
   \,	49
   \-	50
   \.	51
   \/	52
   \:	53
   \;	54
   \<	55
   \=	56
   \>	57
   \?	58
   \@	59
   \[	60
   \\	61
   \]	62
   \^	63
   \_	64
   \`	65
   \{	66
   \|	67
   \}	68
   \~	69
   \5	70
   \6	71
   \7	72
   \8	73
   \9	74))

(defn cifrado-tcs
  [c s]
  (let [f (apply str (flatten (vals (sort (zipmap ((fn [s] (into [] (replace (clave) s)))
                                                   (str/lower-case c))
                                                  (apply mapv vector (partition-all (count (str/lower-case c))
                                                                                    (str/split (apply str s (repeat (- (count (str/lower-case c))                                                                                                      (mod (count s) (count (str/lower-case c)))) " ")) #""))))))))]
    f))

(defn descifrado-tcs
  [x y]
  (let [g (zipmap (vec (sort ((fn [s] (into [] (replace (clave) s))) (str/lower-case x)))) (apply mapv vector (apply mapv vector (partition (/ (count y) (count (str/lower-case x))) (str/split y #"")))))
        f (apply str (flatten (apply mapv vector (replace g ((fn [s] (into [] (replace (clave) s))) (str/lower-case x))))))]
    f))

(defn -main
  [operacion palabra archivoa archivob]
  (if (or (= "" operacion) (= "" palabra) (= "" archivoa) (= "" archivob))
    (println "Error. Falta algún argumento.") 
    (if (and (= operacion "cifrado") (>= (count palabra) 8))
      (spit archivoa (cifrado-tcs palabra (slurp archivob)))
      (if (and (= operacion "descifrado") (>= (count palabra) 8))
        (spit archivoa (descifrado-tcs palabra (slurp archivob)))
        (println "Se desconoce el tipo de operacion ingresado o la palabra no tiene una longitud mayor o igual a 8")))))