(ns plf08.cuadradopolibio)

(defn cuadrado-cifrar
  []
  (hash-map
   \a "♜♜"
   \á "♜♞"
   \b "♜♝"
   \c "♜♛"
   \d "♜♚"
   \e "♜♖"
   \é "♜♘"
   \f "♜♗"
   \g "♜♕"
   \h "♜♔"
   \i "♞♜"
   \í "♞♞"
   \j "♞♝"
   \k "♞♛"
   \l "♞♚"
   \m "♞♖"
   \n "♞♘"
   \ñ "♞♗"
   \o "♞♕"
   \ó "♞♔"
   \p "♝♜"
   \q "♝♞"
   \r "♝♝"
   \s "♝♛"
   \t "♝♚"
   \u "♝♖"
   \ú "♝♘"
   \ü "♝♗"
   \v "♝♕"
   \w "♝♔"
   \x "♛♜"
   \y "♛♞"
   \z "♛♝"
   \0 "♛♛"
   \1 "♛♚"
   \2 "♛♖"
   \3 "♛♘"
   \4 "♛♗"
   \! "♛♕"
   \¡ "♛♕"
   \" "♛♔"
   \# "♚♜"
   \$ "♚♞"
   \% "♚♝"
   \& "♚♛"
   \' "♚♚"
   \( "♚♖"
   \) "♚♘"
   \* "♚♗"
   \+ "♚♕"
   \, "♚♔"
   \- "♖♜"
   \. "♖♞"
   \/ "♖♝"
   \: "♖♛"
   \; "♖♚"
   \< "♖♖"
   \= "♖♘"
   \> "♖♗"
   \? "♖♕"
   \¿ "♖♕"
   \@ "♖♔"
   \[ "♘♜"
   \\ "♘♞"
   \] "♘♝"
   \^ "♘♛"
   \_ "♘♚"
   \` "♘♖"
   \{ "♘♘"
   \| "♘♗"
   \} "♘♕"
   \~ "♘♔"
   \5 "♗♜"
   \6 "♗♞"
   \7 "♗♝"
   \8 "♗♛"
   \9 "♗♚"
   \A "♗♖"
   \B "♗♘"
   \C "♗♗"
   \D "♗♕"
   \E "♗♔"
   \F "♕♜"
   \G "♕♞"
   \H "♕♝"
   \I "♕♛"
   \J "♕♛"
   \K "♕♚"
   \L "♕♖"
   \M "♕♘"
   \N "♕♗"
   \Ñ "♕♗"
   \O "♕♕"
   \P "♕♔"
   \Q "♔♜"
   \R "♔♞"
   \S "♔♝"
   \T "♔♛"
   \U "♔♚"
   \V "♔♖"
   \W "♔♘"
   \X "♔♗"
   \Y "♔♕"
   \Z "♔♔"))

(defn cuadrado-descifrar
  []
  (hash-map
   "♜♜"	\a
   "♜♞"	\á
   "♜♝"	\b
   "♜♛"	\c
   "♜♚"	\d
   "♜♖"	\e
   "♜♘"	\é
   "♜♗"	\f
   "♜♕"	\g
   "♜♔"	\h
   "♞♜"	\i
   "♞♞"	\í
   "♞♝"	\j
   "♞♛"	\k
   "♞♚"	\l
   "♞♖"	\m
   "♞♘"	\n
   "♞♗"	\ñ
   "♞♕"	\o
   "♞♔"	\ó
   "♝♜"	\p
   "♝♞"	\q
   "♝♝"	\r
   "♝♛"	\s
   "♝♚"	\t
   "♝♖"	\u
   "♝♘"	\ú
   "♝♗"	\ü
   "♝♕"	\v
   "♝♔"	\w
   "♛♜"	\x
   "♛♞"	\y
   "♛♝"	\z
   "♛♛"	\0
   "♛♚"	\1
   "♛♖"	\2
   "♛♘"	\3
   "♛♗"	\4
   "♛♕"	\¡
   "♛♕"	\!
   "♛♔"	\"
   "♚♜"	\#
   "♚♞"	\$
   "♚♝"	\%
   "♚♛"	\&
   "♚♚"	\'
   "♚♖"	\(
   "♚♘"	\)
   "♚♗"	\*
   "♚♕"	\+
   "♚♔" \,
   "♖♜"	\-
   "♖♞"	\.
   "♖♝"	\/
   "♖♛"	\:
   "♖♚"	\;
   "♖♖"	\<
   "♖♘"	\=
   "♖♗"	\>
   "♖♕"	\?
   "♖♕"	\¿
   "♖♔"	\@
   "♘♜"	\[
   "♘♞"	\\
   "♘♝"	\]
   "♘♛"	\^
   "♘♚"	\_
   "♘♖"	\`
   "♘♘"	\{
   "♘♗"	\|
   "♘♕"	\}
   "♘♔"	\~
   "♗♜"	\5
   "♗♞"	\6
   "♗♝"	\7
   "♗♛"	\8
   "♗♚"	\9
   "♗♖" \A
   "♗♘" \B
   "♗♗" \C
   "♗♕" \D
   "♗♔" \E
   "♕♜" \F
   "♕♞" \G
   "♕♝" \H
   "♕♛" \I
   "♕♛" \J
   "♕♚" \K
   "♕♖" \L
   "♕♘" \M
   "♕♗" \N
   "♕♗" \Ñ
   "♕♕" \O
   "♕♔" \P
   "♔♜" \Q
   "♔♞" \R
   "♔♝" \S
   "♔♛" \T
   "♔♚" \U
   "♔♖" \V
   "♔♘" \W
   "♔♗" \X
   "♔♕" \Y
   "♔♔" \Z))

(defn cifrar
  [s]
  (let [f (into [] s)]
    (apply str (replace (cuadrado-cifrar) f))))

(defn descifrar
  [s]
  (let [z (apply str (replace (hash-map \space "  ") s))
        f (vec (take-nth 2 (map str z (drop 1 z))))
        x (replace (cuadrado-descifrar) f)
        r (apply str (replace (hash-map "  " " ") x))]
    r))

(defn -main
  [operacion archivoa archivob]
  (if (or (= "" operacion) (= "" archivoa) (= "" archivob))
    (println "Error. Falta algún argumento.")
    (if (= operacion "cifrado")
      (spit archivoa (cifrar (slurp archivob)))
      (if (= operacion "descifrado")
        (spit archivoa (descifrar (slurp archivob)))
        (println "Se desconoce el tipo de operacion ingresado.")))))

