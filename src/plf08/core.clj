(ns plf08.core
  (:gen-class)
  (:require [plf08.cuadradopolibio :as cpolibio])
  ;(:require [plf08.transposicionsimple :as tsimple])
  (:require [plf08.transposicionsimplenuevo :as tcsimple]))


(defn -main
  [metodo operacion archivoa archivob palabra]
  (if (and (empty? metodo) (or (not= metodo "cuadrado de polibio") (not= metodo "transposicion simple")))
    (println "Error, los métodos ingresados son incorrectos")
    (if (= metodo "cuadrado de polibio")
      (cpolibio/-main operacion archivoa archivob)
      (tcsimple/-main operacion palabra archivoa archivob))))

